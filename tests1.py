#!/usr/bin/python
import os
import shutil
import stat
import sys

import unittest


def file_dir(dirs=0, files=0):
    list_df={}
    for i in range(dirs):
        name_dir="dir_%s" % i
        os.mkdir(name_dir)
        list_df[name_dir]=('d',os.getcwd()+'/'+name_dir)
    for j in range(files):
        name_file="file_%s.txt" % j
        f=open(name_file,'w')
        f.close()
        list_df[name_file]=('f',os.getcwd()+'/'+name_file)
    return list_df

class Tests(unittest.TestCase):
    def setUp(self):
        os.chdir('/mnt/users')
        os.mkdir('empty_dir_1')
        os.mkdir('empty_dir_2')
        os.mkdir('dir_with_files1')
        os.mkdir('dir_with_files2')
        os.chdir('/mnt/users/dir_with_files1')
        for i in range(6):
            file_name='file_%s.txt' % i
            f=open(file_name,'w')
            f.close()
        os.chdir('/mnt/users/dir_with_files2')
        for i in range(6):
            file_name='file_%r.txt' % i
            f=open(file_name,'w')
            f.close()
        os.chdir('/mnt/users')
        os.mkdir('dir_tree')
        os.chdir('/mnt/users/dir_tree')
        lib1=file_dir(3,3)        
        list_dirs = [v[1] for k,v in lib1.items() if v[0]=='d']
        for paths in list_dirs:
            os.chdir(paths)
            lib2=file_dir(3,3)
            list_dirs2 = [v[1] for k,v in lib2.items() if v[0]=='d']
            for path in list_dirs2:
                os.chdir(path)
                lib3=file_dir(3,3)

        os.chdir('/mnt/users')
        os.mkdir('dir_links')
        os.chdir('dir_links')
        for i in range(3):
            file_name="for_link_%r.txt" % i
            f=open(file_name, 'w')
            f.close()
            syml_name="link_%r" % i
            os.symlink(file_name, syml_name)
            syml2_name='link_for_link_%r' % i
            os.symlink(syml_name, syml2_name)
        os.chdir('/mnt/users')
        os.mkdir('dir_fifo')
        os.chdir('dir_fifo')
        
    
    def test_right(self):
        os.chmod('/mnt/users/empty_dir_1',stat.S_IRWXU)
        os.chdir('/mnt/users/empty_dir_1')
        f=open('1.txt','w')
        self.assertIsNotNone(f)
        f.close()
        try:
            os.chown('/mnt/users/empty_dir_1',1000,1000)
        except OSError:
            self.assertIsNone(None)

        
unittest.main()
#x=Something()
#x.SetUp()

